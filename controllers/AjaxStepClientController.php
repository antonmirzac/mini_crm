<?php

namespace app\controllers;

use Yii;
use app\models\StepClient;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AjaxStepClientController implements the CRUD actions for StepClient model.
 */
class AjaxStepClientController extends \yii\web\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StepClient models.
     * @return mixed
     */
    public function actionIndex()
    {

    }

    /**
     * Creates a new StepClient model with ajax.
     * If creation is successful, will be returned $model object
     * @return mixed
     */
    public function actionCreate($boardId)
    {
        $model = new StepClient();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        } else {

            $model->board_id = $boardId;

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StepClient model.
     * If update is successful, will be eturned $model object
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $boardId)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing StepClient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return true;
    }

    /**
     * Finds the StepClient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StepClient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StepClient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
