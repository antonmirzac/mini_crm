<?php

namespace app\controllers;

use app\models\Orders;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Steps;
use app\models\Board;
use app\models\BoardOrder;

class SiteController extends Controller
{

    public $wrapperClass;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['boards-order, board-detail-order'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return !Yii::$app->user->identity->candelete;
                        }
                    ],
                ],
            ],
        ];
    }    public function init() {
        parent::init();

    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->wrapperClass = 'desc-page';

        $stepsObj = new Steps();
        $stepOrders = $stepsObj->getOrdersWithSteps();

        return $this->render('index', [
            'steps' => $stepOrders,
        ]);
    }

    public function actionBoardsClient()
    {
        $boards = Board::find()->all();

        return $this->render('boards-client', [
            'boards' => $boards,
        ]);
    }

    public function actionBoardDetailClient($id)
    {
        $this->wrapperClass = 'desc-page';

        $boardObj = new Board();
        $board = $boardObj->getStepsWithClients($id);

        return $this->render('board-item-client', [
            'board' => $board,
            'boardId' => $id,
        ]);
    }

    public function actionBoardsOrder()
    {
        $boards = BoardOrder::find()->all();

        return $this->render('boards-order', [
            'boards' => $boards,
        ]);
    }

    public function actionBoardDetailOrder($id)
    {
        $this->wrapperClass = 'desc-page';

        $boardObj = new BoardOrder();
        $steps = $boardObj->getStepsWithOrders($id);

        return $this->render('board-item-order', [
            'steps' => $steps,
            'boardId' => $id,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
