<?php

namespace app\controllers;

use Yii;
use app\models\Clients;

class AjaxClientController extends \yii\web\Controller
{
    public function actionCreate()
    {
        $model = new Clients();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }
}