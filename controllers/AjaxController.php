<?php

namespace app\controllers;

use app\models\Clients;
use app\models\File;
use Yii;
use app\models\Orders;
use yii\web\UploadedFile;
use app\models\Directory;
use app\models\UploadForm;
Use yii\filters\AccessControl;

class AjaxController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'signup'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];

    }

    public function actionUpload()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $order = new Orders();
        $model = new UploadForm();

        $orderId = Yii::$app->request->post()['orderId'];

        if($orderId <= 0) {
            $orderId = $order->getLastId();
        }

        if (Yii::$app->request->isPost) {
            $model->anyFiles = UploadedFile::getInstances($model, 'anyFiles');
            if($path = $model->upload(new Directory(), $orderId)) {
                $response = ['path' => $path];
                return $response;
            }
        }

        $response = ['error' => 'Upload faile'];
        return $response;
    }

    public function actionRemoveFile($id) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $file = File::findOne($id);
        if($file->delete()) {
            return true;
        } else {
            return false;
        }
    }

    public function actionUpdateStep($orderId, $stepId) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            if (($order = Orders::findOne($orderId)) !== null) {

                $order->step_id = $stepId;
                if($order->save()) {
                    return true;
                }
            }
        }

        return false;
    }

    public function actionUpdateStepClient($clientId, $stepId)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isPost) {
            if (($client = Clients::findOne($clientId)) !== null) {

                $client->step_client_id = $stepId;
                if($client->save()) {
                    return true;
                }
            }
        }

        return false;
    }
}
