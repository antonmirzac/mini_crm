var modalBody = $('body .modal-body');

//Кнопка добавления клиента на странице /orders/create
$('#add-client').click(function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    var modalContainer = $('#user-modal');
    var modalBody = modalContainer.find('.modal-body');

    if(url) {
        $.ajax({ type: "POST",
            url: url,
            success: function(data){
                $(modalBody).html(data);
                modalContainer.modal('show');
            }
        });
    }
});

//Форма добавления клиента на странице /orders/create
modalBody.on('beforeSubmit', $('#ajax-client-form'), function() {
    var clientForm = $('#ajax-client-form');
    if(clientForm.find('.has-error').length) {
        return false;
    }

    var url =  clientForm.attr('action');
    var dataSend = clientForm.serialize();

    $.ajax({
        url: url,
        type: 'post',
        data: dataSend,
        success: function(data) {

            if(data.id) {
                $('#orders-client_id')
                    .append($("<option></option>")
                        .attr("value",data.id)
                        .text(data.name));

                $('#user-modal').modal('hide');
            } else {
                $('#error-msg').html('Ошибка создания клиента');
            }
        },
        error: function (error) {
            $('#error-msg').html(error);
        }
    });

}).on('submit', function(e){
    e.preventDefault();
});