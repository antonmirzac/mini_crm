var modalBody = $('body .modal-body');

//кнопка создания новой доски
$('#create-board-order').click(function (e) {
    e.preventDefault();

    var url = $(this).attr('href');
    var modalContainer = $('#board-order-modal');
    var modalBody = modalContainer.find('.modal-body');

    if(url) {
        $.ajax({ type: "POST",
            url: url,
            success: function(data){
                $(modalBody).html(data);
                modalContainer.modal('show');
            }
        });
    }
});

//форма создания новой доски
modalBody.on('beforeSubmit', $('#ajax-board-order-form'), function() {

    var clientForm = $('#ajax-board-order-form');
    if(clientForm.find('.has-error').length) {
        return false;
    }

    var url =  clientForm.attr('action');
    var dataSend = clientForm.serialize();

    $.ajax({
        url: url,
        type: 'post',
        data: dataSend,
        success: function(data) {

            if(data.id) {
                updateBoardsOrderList();
                $('#board-order-modal').modal('hide');
            } else {
                $('#error-msg').html('Ошибка создания этапа');
            }
        },
        error: function (error) {
            $('#error-msg').html(error);
        }
    });

}).on('submit', function(e){
    e.preventDefault();
});

function updateBoardsOrderList() {
    $.ajax({ type: "POST",
        url: 'ajax-board-order/index',
        success: function(data){
            $('#boards-order-list-ajax').html(data);
        }
    });
}