var modalBody = $('body .modal-body');

//кнопка создания этапа
$('#create-step-order').click(function (e) {
    e.preventDefault();

    var url = $(this).attr('href');
    var modalContainer = $('#step-order-modal');
    var modalBody = modalContainer.find('.modal-body');

    if(url) {
        $.ajax({ type: "POST",
            url: url,
            success: function(data){
                $(modalBody).html(data);
                modalContainer.modal('show');
            }
        });
    }
});

//форма создания этапа
modalBody.on('beforeSubmit', $('#step-order-modal'), function() {

    var clientForm = $('#step-order-form');
    if(clientForm.find('.has-error').length) {
        return false;
    }

    var url =  clientForm.attr('action');
    var dataSend = clientForm.serialize();



    $.ajax({
        url: url,
        type: 'post',
        data: dataSend,
        success: function(data) {

            if(data.id) {
                $('#step-order-modal').modal('hide');
                updateStepOrderList();
            } else {
                $('#error-msg').html('Ошибка создания');
            }
        },
        error: function (error) {
            $('#error-msg').html(error);
        }
    });

}).on('submit', function(e){
    e.preventDefault();
});

function updateStepOrderList() {
    location.reload();
}