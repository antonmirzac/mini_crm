// Select multiply technologies in Developers CRUD
var selectTechnologies = $('#input_technologies');
var selectTechnologiesVal = selectTechnologies.val();
var technologiesId = selectTechnologiesVal ? selectTechnologiesVal : [];


selectTechnologies.on('select2:select', function (e) {
    if(technologiesId !== null && technologiesId.indexOf(e.params.data.id) === -1) {
        technologiesId.push(e.params.data.id);
        setJsonTechnologies(technologiesId);
    }
}).on('select2:unselect', function (e) {
    var isIsset = technologiesId.indexOf(e.params.data.id);
    if( isIsset !== -1) {
        technologiesId.remove(e.params.data.id);
        setJsonTechnologies(technologiesId);
    }
});

//set json string for #developers-technologies
function setJsonTechnologies(arr) {
    var jsonTechnologies = JSON.stringify(toObject(arr));
    $('#developers-technologies').val(jsonTechnologies);
    console.log(jsonTechnologies);
}

// Delete element from array
Array.prototype.remove = function(value) {
    var idx = this.indexOf(value);
    if (idx != -1) {
        return this.splice(idx, 1);
    }
    return false;
}

//convert array to object
function toObject(arr) {
    var rv = {};
    for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
    return rv;
}


//remove files from order
var removeFile = $(".remove-file");
removeFile.click(function(e){
    e.preventDefault();
    var file = $(this);
    var url = file.attr('href');

    $.ajax({ type: "POST",
        url: url,
        success: function(data){
            file.parent('.file-item').remove();
        }
    });
});