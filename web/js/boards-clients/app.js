/*-----/boards-client--------*/
var modalBody = $('body .modal-body');

//Кнопка добавления доски на странице /boards-client
$('#create-board').click(function (e) {
    e.preventDefault();

    var url = $(this).attr('href');
    var modalContainer = $('#board-modal');
    var modalBody = modalContainer.find('.modal-body');

    if(url) {
        $.ajax({ type: "POST",
            url: url,
            success: function(data){
                $(modalBody).html(data);
                modalContainer.modal('show');
            }
        });
    }
});

//Форма добавления доски на странице /boards-client
modalBody.on('beforeSubmit', $('#ajax-board-form'), function() {

    var clientForm = $('#ajax-board-form');
    if(clientForm.find('.has-error').length) {
        return false;
    }

    var url =  clientForm.attr('action');
    var dataSend = clientForm.serialize();

    $.ajax({
        url: url,
        type: 'post',
        data: dataSend,
        success: function(data) {

            if(data.id) {
                updateBoardsList();
                $('#board-modal').modal('hide');
            } else {
                $('#error-msg').html('Ошибка создания этапа');
            }
        },
        error: function (error) {
            $('#error-msg').html(error);
        }
    });

}).on('submit', function(e){
    e.preventDefault();
});

//ф-я обновления списка досок на странице /boards-client
function updateBoardsList() {
    $.ajax({ type: "POST",
        url: 'ajax-board-client/index',
        success: function(data){
            $('#boards-list-ajax').html(data);
        }
    });
}