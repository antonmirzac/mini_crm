var modalBody = $('body .modal-body');

//Кнопка добавления этапа клиента
$('#create-step-client').click(function (e) {
    e.preventDefault();

    var url = $(this).attr('href');
    var modalContainer = $('#step-client-modal');
    var modalBody = modalContainer.find('.modal-body');

    if(url) {
        $.ajax({ type: "POST",
            url: url,
            success: function(data){
                $(modalBody).html(data);
                modalContainer.modal('show');
            }
        });
    }
});

//Форма добавления этапа клиента
modalBody.on('beforeSubmit', $('#step-client-modal'), function() {
    console.log('step-client-modal');
    var clientForm = $('#step-client-form');
    if(clientForm.find('.has-error').length) {
        return false;
    }

    var url =  clientForm.attr('action');
    var dataSend = clientForm.serialize();



    $.ajax({
        url: url,
        type: 'post',
        data: dataSend,
        success: function(data) {

            if(data.id) {
                $('#step-client-modal').modal('hide');
                updateStepClientList();
            } else {
                $('#error-msg').html('Ошибка создания');
            }
        },
        error: function (error) {
            $('#error-msg').html(error);
        }
    });

}).on('submit', function(e){
    e.preventDefault();
});

//ф-я обновления списка этапов клиента
function updateStepClientList() {
    location.reload();
}