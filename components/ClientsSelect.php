<?php

namespace app\components;

use app\components\SelectWidget;
use app\models\Clients;

class ClientsSelect extends SelectWidget
{
    public function run()
    {
        $this->data = Clients::find()->select(['id', 'name'])->indexBy('id')->asArray()->all();
        $this->menuHtml = $this->getMenuHtml($this->data);
        return $this->menuHtml;
    }
}