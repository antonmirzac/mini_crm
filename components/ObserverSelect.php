<?php

namespace app\components;

use app\models\User;

class ObserverSelect extends SelectWidget
{
    public function run()
    {
        $this->data = User::find()->select(['id', 'username as name'])->indexBy('id')->asArray()->all();
        $this->menuHtml = $this->getMenuHtml($this->data);
        return $this->menuHtml;
    }
}
