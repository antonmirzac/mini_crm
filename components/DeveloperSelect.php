<?php

namespace app\components;

use app\models\Developers;

class DeveloperSelect extends SelectWidget
{
    public function run()
    {
        $this->data = Developers::find()->select(['id', 'name'])->indexBy('id')->asArray()->all();
        $this->menuHtml = $this->getMenuHtml($this->data);
        return $this->menuHtml;
    }
}