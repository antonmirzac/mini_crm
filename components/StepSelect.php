<?php

namespace app\components;

use app\models\Steps;

class StepSelect extends SelectWidget
{
    public function run()
    {
        $this->data = Steps::find()->select(['id', 'title as name'])->indexBy('id')->asArray()->all();
        $this->menuHtml = $this->getMenuHtml($this->data);
        return $this->menuHtml;
    }
}
