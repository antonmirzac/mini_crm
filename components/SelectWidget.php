<?php

namespace app\components;

use yii\base\Widget;


class SelectWidget extends Widget
{
    public $tpl;
    public $model;
    public $data;
    public $menuHtml;

    public function init(){
        parent::init();
        if( $this->tpl === null ){
            $this->tpl = 'select';
        }
        $this->tpl .= '.php';
    }

    protected function getMenuHtml($data){
        $str = '';
        foreach ($data as $item) {
            $str .= $this->catToTemplate($item);
        }
        return $str;
    }

    protected function catToTemplate($item){
        ob_start();
        include __DIR__ . '/menu_tpl/' . $this->tpl;
        return ob_get_clean();
    }

}