<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Developers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="developers-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-4"><?= $form->field($model, 'nick')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-4"><?= $form->field($model, 'skype')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-4"><?= $form->field($model, 'email')->textInput() ?></div>
        <div class="col-md-4">
            <div class="form-group field-input_technologies">
                <label class="control-label" for="developers-input_technologies">Технологии</label>
                <?=Select2::widget([
                    'name' => 'input_technologies',
                    'id' => 'input_technologies',
                    'language' => 'ru',
                    'value' => $model->selectTechnologies,
                    'data' => $model->allTechnologies,
                    'options' => ['multiple' => true],
                    'showToggleAll' => false,
                ]);?>
            </div>
        </div>
        <?= $form->field($model, 'technologies')->hiddenInput(['rows' => 6])->label(false) ?>
    </div>
    <div class="row">
        <div class="col-md-3"><?= $form->field($model, 'valuation')->textInput() ?></div>
        <div class="col-md-3"><?= $form->field($model, 'experience')->textInput() ?></div>
        <div class="col-md-3"><?= $form->field($model, 'mode_work')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-3"><?= $form->field($model, 'cost_of')->textInput(['maxlength' => true]) ?></div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
