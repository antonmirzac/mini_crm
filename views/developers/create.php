<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Developers */

$this->title = 'Добавить программиста';
$this->params['breadcrumbs'][] = ['label' => 'Программисты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="developers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
