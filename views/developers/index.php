<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Technologies;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Программисты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="developers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'nick',
            'skype',
            'phone',
            'email:email',
            [
                'attribute' => 'technologies',
                'value' => function($data){
                    $technologiesId = Technologies::getByJsonStatic($data->technologies);
                    return implode(",", Technologies::getTitles($technologiesId));
                },
                'format' => 'ntext',
            ],
            'valuation',
            'experience',
            'mode_work',
            'cost_of',
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
