<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use rmrevin\yii\module\Comments;

/* @var $this yii\web\View */
/* @var $model app\models\Developers */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Программисты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="developers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить данные', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'nick',
            'skype',
            'phone',
            'email:email',
            [
                'attribute' => 'technologies',
                'value' => $titlesTechnologies,
                'format' => 'ntext',
            ],
            'valuation',
            'experience',
            'mode_work',
            'cost_of',
        ],
    ]) ?>

    <?php
    echo Comments\widgets\CommentListWidget::widget([
        'entity' => (string) $model->id,
    ]);
    ?>

</div>
