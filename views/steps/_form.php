<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Steps */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="steps-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'sort')->textInput() ?>

        <?= $form->field($model, 'board_id')
            ->dropDownList(\app\models\BoardOrder::find()
                ->select(['title', 'id'])
                ->indexBy('id')
                ->column(), ['prompt' => '']) ?>


        <?= $form->field($model, 'show_in_board')->checkbox([ '0', '1', ], ['prompt' => '']) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
