<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Steps */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Этапы заказов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="steps-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'sort',
            [
                'attribute' => 'show_in_board',
                'value' => !$model->show_in_board ?
                    '<span class="text-danger"><i class="fa fa-times" aria-hidden="true"></i></span>' :
                    '<span class="text-success"><i class="fa fa-check" aria-hidden="true"></i></span>',
                'format' => 'html',
            ],
            [
                'attribute' => 'board_id',
                'value' => ($model->board_id) ? $model->board->title : "",
            ],
        ],
    ]) ?>

</div>
