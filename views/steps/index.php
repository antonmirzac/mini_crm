<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Этапы заказов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="steps-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Содать этап', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'sort',
            [
                'attribute' => 'show_in_board',
                'value' => function($data){
                    return !$data->show_in_board ?
                        '<span class="text-danger"><i class="fa fa-times" aria-hidden="true"></i></span>' :
                        '<span class="text-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'board_id',
                'value' => function($data){
                    return ($data->board_id) ? $data->board->title : "";
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
