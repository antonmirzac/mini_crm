<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomerFrom */

$this->title = 'Форма';
$this->params['breadcrumbs'][] = ['label' => 'Откуда пришел клиент', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-from-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
