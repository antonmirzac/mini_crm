<?php
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use app\assets\OrdersBoardsAsset;

OrdersBoardsAsset::register($this);

$this->title = 'Персональные доски заказов';
?>

    <div class="boards-page-board-section-header">
        <h3 class="boards-page-board-section-header-name"><?= Html::encode($this->title) ?></h3>
    </div>

    <ul class="boards-page-board-section-list" id="">
    <span id="boards-order-list-ajax">
        <?php foreach ($boards as $item):?>
            <li class="boards-page-board-section-list-item">
                <a class="board-tile" href="<?=Url::to(['/site/board-detail-order', 'id' => $item->id])?>">
                    <span class="board-tile-details is-badged">
                        <span title="CRM мини" dir="auto" class="board-tile-details-name"><?=$item->title?></span>
                    </span>
                </a>
            </li>
        <?php endforeach;?>
    </span>
        <li class="boards-page-board-section-list-item">
            <a href="<?=Url::to(['ajax-board-order/create'])?>" class="board-tile mod-add" id="create-board-order">
            <span class="board-tile-details is-badged">
				<span title="CRM мини" dir="auto" class="board-tile-details-name">Создать новую доску…</span>
            </span>
            </a>
        </li>
    </ul>

<?php
Modal::begin([
    'header' => 'Создать новую доску',
    'id' => 'board-order-modal'
]);
Modal::end();
?>