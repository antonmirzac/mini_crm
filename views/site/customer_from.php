<?php

/* @var $this yii\web\View */

$this->title = 'Откуда пришел клиент';
use yii\helpers\Url;
?>



<div class="row">
    <div class="col-md-12">
        <h1>Откуда пришел клиент</h1>
        <table class="table customer-from">
            <thead>
            <tr>
                <th>Откуда пришел</th>
                <th>Количесвто</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($customerFrom as $key=>$item):?>
                <tr>
                    <td><?=$item['customer_from']?></td>
                    <td><?=$item['total']?></td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>