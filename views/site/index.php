<?php

/* @var $this yii\web\View */

$this->title = 'Mini crm';
use yii\helpers\Url;
use kartik\sortable\Sortable;
use yii\helpers\StringHelper;

//dd($steps);
?>
<div class="desc-page-container">
    <div class="page-container-width">
        <div class="row">

            <?php foreach ($steps as $key=>$itemStep):
                $stepTitle = $itemStep['title'];
                $stepId = $itemStep['id'];

                if(isset($itemStep['orders'])) {
                    $countSteps = count($itemStep['orders']);
                } else {
                    $countSteps = 0;
                }

                ?>
                <div class="step-column" id="<?= $key?>step">
                    <div class="step-title">
                        <div class="bold-title"><?=StringHelper::truncate($stepTitle, 24)?></div>
                            <span class="stagevalue">
                                <span class="value">
                                    <span class="all-sum-step"><?= $itemStep['all_sum']?></span> р.
                                    <small>Заказов <span class="count-orders-item"><?= $countSteps?></span></small>
                                </span>
                            </span>
                    </div>
                    <?php
                    $items = [];
                    if($countSteps) {
                        foreach($itemStep['orders'] as $itemOrder) {

                            $orderId = $itemOrder['order_id'];
                            $orderTitle = $itemOrder['order_title'];
                            if(!is_null($itemOrder['full_sum'])) {
                                $full_sum = $itemOrder['full_sum'];
                            } else {
                                $full_sum = 0;
                            }
                            $client_name = $itemOrder['client_name'];
                            $developer_name = $itemOrder['developer_name'];
                            $delivery_date = $itemOrder['delivery_date'];
                            $clear_sum = $itemOrder['clear_sum'];
                            $client_loyalty = $itemOrder['client_loyalty'];
                            $client_id = $itemOrder['client_id'];
                            $deadline = $itemOrder['deadline'];

                            $url = Url::to(['orders/view', 'id' => $orderId]);
                            $client_url = Url::to(['clients/view', 'id' => $client_id]);
                            $items[]['content'] = "
                                <div class='box box-default order-item collapsed-box' id='{$orderId}' data-parent-step='{$key}step'>
                                <div class='deadline {$deadline}'></div>
                                    <div class='box-header'>
                                        <span class='box-title title-order'>{$orderTitle}</span>
                                        <div>
                                            <small><span class='detail value full-sum-item'>{$full_sum} р.</span></small>
                                            <small><span class='detail value client-name-item'>{$client_name}</span></small>
                                                      
                                        </div>
                                        <div class='box-tools pull-right'>
                                            <div class='client_loyalty hidden'>{$client_loyalty}</div>
                                            <div><a href='{$url}'>
                                                <i class='fa fa-eye' aria-hidden='true'></i>
                                            </a></div>
                                            <div><a href='{$client_url}'>
                                                <i class='fa fa-address-card' aria-hidden='true'></i>
                                            </a></div>
                                            <div><a href='#' class='btn btn-box-tool' data-widget='collapse'>
                                                <i class='fa fa-plus'></i>
                                            </a></div>
                                        </div>                               
                                    </div>
                                    <div class='box-body'>
                                        <span class='box-body-text'>Полная сумма: {$full_sum}</span><br>
                                        <span class='box-body-text'>Программист: {$developer_name}</span><br>
                                        <span class='box-body-text'>Дата сдачи: {$delivery_date}</span><br>
                                        <span class='box-body-text'>Чистая прибыль: {$clear_sum}</span><br>
                                    </div>
                                </div>
                               ";
                        }
                    }

                    echo Sortable::widget([
                        'connected'=>true,
                        'options' => [
                            'data-step'=>$stepId,
                            'data-url-update' => Url::to(['ajax/update-step']),
                        ],
                        'items' => $items,
                        'pluginEvents' => [
                            'sortupdate' => "function(e, e2) {
                                var currentElementSelector = $(e2.item.context).find('div');
                                
                                var currentStep = {$stepId};
                                var currentId = currentElementSelector.attr('id');                                
                                var oldStepId =  $(currentElementSelector).data('parent-step');

                                currentElementSelector.data('parent-step', currentStep + 'step');
                             
                                //Пересчет заказов и суммы в этапе
                                var fullSumItem = parseInt(currentElementSelector.find('.full-sum-item').text());
                                if(Number.isNaN(fullSumItem)) {
                                    fullSumItem = 0;
                                }
                                
                                //Этап from
                                var oldStepObj = $('#' + oldStepId);
                                
                                var countOrdersItemFrom = oldStepObj.find('.count-orders-item');
                                var countOrdersItemFromValue = countOrdersItemFrom.text();
                                countOrdersItemFromValue--;
                                countOrdersItemFrom.text(countOrdersItemFromValue);
                                
                                var allSumStepFrom = oldStepObj.find('.all-sum-step');
                                var allSumStepFromValue = parseInt(allSumStepFrom.text()); 
                                allSumStepFrom.text(allSumStepFromValue - fullSumItem);
                              
                                //Этап to
                                var currentStepObj = $('#' + currentStep + 'step');
                                
                                var countOrdersItemTo = currentStepObj.find('.count-orders-item');
                                var countOrdersItemToValue = countOrdersItemTo.text();
                                countOrdersItemToValue++;
                                countOrdersItemTo.text(countOrdersItemToValue);
                                
                                var allSumStepTo = currentStepObj.find('.all-sum-step');
                                var allSumStepToValue = parseInt(allSumStepTo.text()); 
                                allSumStepTo.text(allSumStepToValue + fullSumItem);
                                
                                $.ajax({ type: 'POST',
                                    url: '/ajax/update-step?orderId='+currentId+'&stepId='+currentStep,
                                    success: function(data){
                                        console.log(data);
                                    }
                                });                                
                            }",
                        ],
                    ]);
                    ?>
                </div>
            <?php endforeach;?>
        </div>
    </div>

</div>



<div class="clearfix"></div>