<?php

/* @var $this yii\web\View */

$this->title = (!empty($board)) ? $board['title'] : 'Этапов не найдено';
use yii\helpers\Url;
use kartik\sortable\Sortable;
use yii\helpers\StringHelper;
use yii\bootstrap\Modal;
use app\assets\DetailBoardClient;

DetailBoardClient::register($this);
?>
<div class="desc-page-container">
    <div class="page-container-width">

        <div class="row">
            <?php if(!empty($board['items'])):?>
            <?php foreach ($board['items'] as $key=>$itemStep):
                $stepTitle = $itemStep['title'];
                $stepId = $itemStep['id'];

                if(isset($itemStep['clients'])) {
                    $countSteps = count($itemStep['clients']);
                } else {
                    $countSteps = 0;
                }

                ?>
                <div class="step-column" id="<?= $key?>step">
                    <div class="step-title">
                        <div class="bold-title"><?=StringHelper::truncate($stepTitle, 24)?></div>
                        <span class="stagevalue">
                            <span class="value">
                                <small>Клиентов <span class="count-clients-item"><?= $countSteps?></span></small>
                            </span>
                        </span>
                    </div>
                    <?php
                    $items = [];
                    if($countSteps) {
                        foreach($itemStep['clients'] as $itemClient) {

                            $clientId = $itemClient['client_id'];
                            $clientName = $itemClient['client_name'];
                            $clientPhone = $itemClient['client_cellphone'];
                            $countOrders = $itemClient['count_orders'];
                            $deadline = $itemClient['deadline'];

                            $diff_date = $itemClient['diff_date'];

                            $url = Url::to(['clients/view', 'id' => $clientId]);
                            $items[]['content'] = "
                                <div class='box box-default order-item collapsed-box' id='{$clientId}' data-parent-step='{$key}step'>
                                    <div class='deadline {$deadline}'></div>
                                    <div class='box-header'>
                                        <span class='box-title title-order'>{$clientName}</span>
                                        <div class='box-tools pull-right'>
                                            <div><a href='{$url}'>
                                                <i class='fa fa-eye' aria-hidden='true'></i>
                                            </a></div>
                                            <div><a href='#' class='btn btn-box-tool' data-widget='collapse'>
                                                <i class='fa fa-plus'></i>
                                            </a></div>
                                        </div>
                                                                                
                                    </div>
                                    <div class='box-body'>
                                        <span class='box-body-text'>Телефон: {$clientPhone}</span><br>
                                        <span class='box-body-text'>Количество заказов: {$countOrders}</span><br>
                                    </div>
                                </div>
                               ";
                        }
                    }

                    echo Sortable::widget([
                        'connected'=>true,
                        'options' => [
                            'data-step'=>$stepId,
                            'data-url-update' => Url::to(['ajax/update-step-client']),
                        ],
                        'items' => $items,
                        'pluginEvents' => [
                            'sortupdate' => "function(e, e2) {
                                var currentElementSelector = $(e2.item.context).find('div');
                                
                                var currentStep = {$stepId};
                                var currentId = currentElementSelector.attr('id');                                
                                var oldStepId =  $(currentElementSelector).data('parent-step');
                                
                                currentElementSelector.data('parent-step', currentStep + 'step');
                                                                                             
                                //Этап from
                                var oldStepObj = $('#' + oldStepId);
                                
                                var countClientsItemFrom = oldStepObj.find('.count-clients-item');
                                var countClientsItemFromValue = countClientsItemFrom.text();
                                countClientsItemFromValue--;
                                countClientsItemFrom.text(countClientsItemFromValue);
                                                              
                                //Этап to
                                var currentStepObj = $('#' + currentStep + 'step');
                                
                                var countClientsItemTo = currentStepObj.find('.count-clients-item');
                                var countClientsItemToValue = countClientsItemTo.text();
                                countClientsItemToValue++;
                                countClientsItemTo.text(countClientsItemToValue);
                                
                                $.ajax({ type: 'POST',
                                    url: '/ajax/update-step-client?clientId='+currentId+'&stepId='+currentStep,
                                    success: function(data){
                                        console.log(data);
                                    }
                                });
                            }",
                        ],
                    ]);
                    ?>
                </div>
            <?php endforeach;?>
            <div class="step-column add-client-button-column">
                <a href="<?= Url::to(['ajax-step-client/create', 'boardId' => $boardId])?>" id="create-step-client" class="btn">
                    Создать этап клиента
                </a>
            </div>
            <?php else:?>
                <div class="step-column add-client-button-column">
                    <a href="<?= Url::to(['ajax-step-client/create', 'boardId' => $boardId])?>" id="create-step-client" class="btn">
                        Создать этап клиента
                    </a>
                </div>
            <?php endif;?>
        </div>



    </div>
</div>
<div class="clearfix"></div>
<?php
$this->beginBlock('step-client-modal-block');
?>
<?php
Modal::begin([
    'header' => 'Создать этап клиента',
    'id' => 'step-client-modal'
]);
Modal::end();
?>
<?php
$this->endBlock();
?>
