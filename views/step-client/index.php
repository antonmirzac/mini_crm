<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Этапы клиентов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="step-client-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить этап', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'attribute' => 'board_id',
                'value' => function($data){
                    return $data->board->title;
                },
            ],
            'sort',
            [
                'attribute' => 'show_in_board',
                'value' => function($data){
                    return !$data->show_in_board ?
                        '<span class="text-danger"><i class="fa fa-times" aria-hidden="true"></i></span>' :
                        '<span class="text-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                },
                'format' => 'html',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
