<?php

use yii\helpers\Url;

?>

<?= $this->render('_form', [
    'model' => $model,
    'action' => Url::to(['ajax-step-order/create', 'boardId' => $model->board_id]),
]) ?>