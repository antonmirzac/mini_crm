<?php

use yii\helpers\Url;

?>

<?= $this->render('_form', [
    'model' => $model,
    'action' => Url::to(['ajax-step-client/create', 'boardId' => $model->board_id]),
]) ?>