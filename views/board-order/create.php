<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BoardOrder */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Наименования доски(заказы)', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-order-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
