<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Expenditure */

$this->title = 'Обновить статью расходов: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Статьи расходов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="expenditure-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
