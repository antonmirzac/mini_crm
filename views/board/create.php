<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Board */

$this->title = 'Добавить наименование';
$this->params['breadcrumbs'][] = ['label' => 'Наименования', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
