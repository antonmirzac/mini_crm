<aside class="main-sidebar">

    <section class="sidebar">
        <?php if(!Yii::$app->user->identity->candelete):?>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    ['label' => 'Доски клиентов', 'icon' => 'fa fa-desktop', 'url' => ['/boards-client']],
                    ['label' => 'Доски заказов', 'icon' => 'fa fa-desktop', 'url' => ['/boards-order']],
                    ['label' => 'Доска заказов(old)', 'url' => ['/site/index'], 'icon' => 'fa fa-desktop'],
                    ['label' => 'Заказы', 'url' => ['/orders/index'], 'icon' => 'fa fa-calendar-check-o'],
                    ['label' => 'Клиенты', 'url' => ['/clients/index'], 'icon' => 'fa fa-handshake-o'],
                    ['label' => 'Программисты', 'url' => ['/developers/index'], 'icon' => 'fa fa-laptop'],
                    ['label' => 'Касса', 'icon' => 'fa fa-money', 'url' => ['/costs/index']],
                    [
                        'label' => 'Справочники',
                        'icon' => 'fa fa-book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Этапы', 'icon' => 'fa fa-list-alt', 'url' => ['/steps/index']],
                            ['label' => 'Технологии', 'icon' => 'fa fa-tags', 'url' => ['/technologies/index']],
                            ['label' => 'Откуда пришел клиент', 'icon' => 'fa fa-globe', 'url' => ['/customer-from/index']],
                            ['label' => 'Пользователи', 'icon' => 'fa fa-user', 'url' => ['/user/index']],
                            ['label' => 'Компании', 'icon' => 'fa fa-building', 'url' => ['/company/index']],
                            ['label' => 'Статья движений', 'icon' => 'fa fa-area-chart', 'url' => ['/expenditure/index']],
                            ['label' => 'Наименов. доски(клиенты)', 'icon' => 'fa fa-desktop', 'url' => ['/board/index']],
                            ['label' => 'Наименов. доски(заказы)', 'icon' => 'fa fa-desktop', 'url' => ['/board-order/index']],
                            ['label' => 'Этапы клиентов', 'icon' => 'fa fa-step-forward', 'url' => ['/step-client/index']],
                        ],
                    ],
                    //['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    //['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    ['label' => 'Выйти', 'url' => ['site/logout'], 'visible' => !Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

        <?php else:?>
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Меню', 'options' => ['class' => 'header']],
                        ['label' => 'Доски клиентов', 'icon' => 'fa fa-desktop', 'url' => ['/boards-client']],
                        ['label' => 'Клиенты', 'url' => ['/clients/index'], 'icon' => 'fa fa-handshake-o'],
                        [
                            'label' => 'Справочники',
                            'icon' => 'fa fa-book',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Этапы клиентов', 'icon' => 'fa fa-step-forward', 'url' => ['/step-client/index']],
                            ],
                        ],
                        //['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                        //['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                        ['label' => 'Выйти', 'url' => ['site/logout'], 'visible' => !Yii::$app->user->isGuest],
                    ],
                ]
            ) ?>

        <?php endif;?>
    </section>

</aside>
