<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\StringHelper;
use rmrevin\yii\module\Comments;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="orders-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            [
                'attribute' => 'project_link',
                'value' => ($model->project_link) ?
                    Html::a(StringHelper::truncate($model->project_link, 40), $model->project_link, ['target'=>'_blank'])
                    : "",
                'format' => 'html',
            ],
            'accesses:ntext',
            'full_sum',
            'clear_sum',
            'payed_sum',
            'count_steps',
            'created_at',
            'updated_at',
            'delivery_date',
            [
                'attribute' => 'client_id',
                'value' => ($model->client) ? $model->client->name : "",
            ],
            [
                'attribute' => 'developer_id',
                'value' => ($model->developer) ? $model->developer->name : "",
            ],
            [
                'attribute' => 'step_id',
                'value' => ($model->step) ? $model->step->title : "",
            ],
            [
                'attribute' => 'observer_id',
                'value' => ($model->observer) ? $model->observer->username : "",
            ],
            [
                'attribute' => 'files',
                'value' => $filesContent,
                'format' => 'html',
            ],
        ],
    ]) ?>

    <?php
    echo Comments\widgets\CommentListWidget::widget([
        'entity' => (string) $model->id,
    ]);
    ?>

</div>
