<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\File;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать заказ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'responsive'=>true,
                'hover'=>true,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'title',
                        'value' => function($data){
                            $title = StringHelper::truncate($data->title, 30);
                            $link = ($data->project_link) ? Html::a('Ссылка', $data->project_link, ['target'=>'_blank']) : "";
                            return "
                                <div>{$title}</div>
                                <div>{$link}</div>
                            ";
                        },
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'sum_group',
                        'value' => function($data){
                            $full_sum = ($data->project_link) ? $data->full_sum : '<span class="not-set">(не задано)</span>';
                            $clear_sum = ($data->clear_sum) ? $data->clear_sum : '<span class="not-set">(не задано)</span>';
                            $payed_sum = ($data->payed_sum) ? $data->payed_sum : '<span class="not-set">(не задано)</span>';
                            return "
                                <div>Сумма общая: {$full_sum}</div>
                                <div>Сумма чистая: {$clear_sum}</div>
                                <div>Сумма оплаченная: {$payed_sum}</div>
                                ";
                        },
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'participants',
                        'value' => function($data){
                            $developer = ($data->developer) ? $data->developer->nick : '<span class="not-set">(не задано)</span>';
                            $observer = ($data->observer) ? $data->observer->username : '<span class="not-set">(не задано)</span>';
                            $client = ($data->client) ? $data->client->name : '<span class="not-set">(не задано)</span>';
                            return "
                                <div>Исполнитель: {$developer}</div>
                                <div>Наблюдатель: {$observer}</div>
                                <div>Клиент: {$client}</div>
                                ";
                        },
                        'format' => 'html',
                    ],
                    'count_steps',
                    //'created_at',
                    //'updated_at',
                    [
                        'attribute' => 'step_id',
                        'value' => function($data){
                            return ($data->step) ? $data->step->title : "";
                        },
                    ],
                    [
                        'attribute' => 'files',
                        'format' => 'html',
                        'value' => function($data){
                            return File::getFilesContent($data->id);
                        },
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
    </div>

</div>
