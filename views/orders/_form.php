<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;
use yii\bootstrap\Modal;
use app\assets\AjaxClientsAsset;

AjaxClientsAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data',
            ]
    ]) ?>

    <?= $form->field($model, 'tempFiles')->hiddenInput()->label(false) ?>
    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'project_link')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <?= $form->field($model, 'accesses')->textarea(['rows' => 3]) ?>

            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($model, 'full_sum')->textInput() ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'clear_sum')->textInput() ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'payed_sum')->textInput() ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'count_steps')->textInput() ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'developer_id')
                        ->dropDownList(\app\models\Developers::find()
                            ->select(['nick', 'id'])
                            ->indexBy('id')
                            ->column(), ['prompt' => '']) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'step_id')
                        ->dropDownList(\app\models\BoardOrder::getBoardsWithSteps(), ['prompt' => 'Выберите этап']) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'observer_id')
                        ->dropDownList(\app\models\User::find()
                            ->select(['username as name', 'id'])
                            ->indexBy('id')
                            ->column(), ['prompt' => '']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'client_id')
                        ->dropDownList(\app\models\Clients::find()
                            ->select(['name', 'id'])
                            ->indexBy('id')
                            ->column(), ['prompt' => '']) ?>
                </div>
                <div class="col-md-3">
                    <div class="add-client">
                        <a href="<?= Url::to(['ajax-client/create'])?>" id="add-client" class="btn btn-primary">
                            Создать клиента
                        </a>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>


        </div>
        <div class="col-md-2">

            <?= $form->field($model, 'delivery_date')->widget(
                DatePicker::className(), [
                'inline' => true,
                'language' => 'ru',
                'template' => '{input}',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);?>
        </div>


        <div class="col-md-9">
            <?php $form2= ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

            <?= $form2->field($modelUpload, 'anyFiles[]')->widget(FileInput::classname(), [
                'options' => ['multiple' => true],
                'pluginOptions' => [
                    'uploadUrl' => Url::to(['/ajax/upload']),
                    'uploadExtraData' => [
                        'orderId' => $model->id,
                    ],
                    'showPreview' => true,
                    'showCaption' => true,
                    'showRemove' => false,
                    'showUpload' => true,
                    'previewFileType' => 'any',
                    'maxFileSize' => 20971520,
                ],
                'pluginEvents' => [
                    'fileuploaded' => "
                        function(event, data, previewId, index) {                    
                            var image = {
                              name: data.files[index].name,
                              path: data.response.path,
                            };
                           
                            console.log(image);
                            console.log(index);
                            console.log(data.files[index].name);
                            
                            var resultJsonInput = $('#orders-tempfiles').val();
                            if(resultJsonInput.length > 0) {
                                var resultArray = JSON.parse(resultJsonInput);
                            } else {
                                var resultArray = Array();
                            }
                            resultArray.push(index);
                            resultArray[index] = image;
                            var JsonResult = JSON.stringify(resultArray);
                            $('#orders-tempfiles').val(JsonResult);
                        }
                    ",
                ],
            ]);?>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

<?php
Modal::begin([
    'header' => 'Создать клиента',
    'id' => 'user-modal'
]);
Modal::end();
?>