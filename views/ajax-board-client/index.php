<?php
use yii\helpers\Url;
?>

<?php foreach ($boards as $item):?>
    <li class="boards-page-board-section-list-item">
        <a class="board-tile" href="<?=Url::to(['/site/board-detail-client', 'id' => $item->id])?>">
            <span class="board-tile-details is-badged">
				<span title="CRM мини" dir="auto" class="board-tile-details-name"><?=$item->title?></span>
            </span>
        </a>
    </li>
<?php endforeach;?>