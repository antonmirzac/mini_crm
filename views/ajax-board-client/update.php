<?php

use yii\helpers\Url;

?>

<?= $this->render('_form', [
    'model' => $model,
    'action' => Url::to(['ajax-board-client/update']),
]) ?>