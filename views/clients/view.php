<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use rmrevin\yii\module\Comments;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить данные', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'city',
            'main_feedback',
            'cellphone',
            'skype',
            'email:email',
            'telegram',
            'loyalty',
            'date_next_contact',
            [
                'attribute' => 'customer_from',
                'value' => ($model->customerfrom) ? $model->customerfrom->title : "",
            ],
            'vk',
            'info',
            [
                'attribute' => 'step_client_id',
                'value' => ($model->step_client_id) ? $model->stepClient->title : "",
            ],
            [
                'attribute' => 'board',
                'value' => ($model->step_client_id) ? $model->stepClient->board->title : "",
            ],
            [
                'attribute' => 'contacts',
                'value' => \app\models\Clients::contactsArrToHtml($model->contacts),
                'format' => 'html',
            ],

        ],
    ]) ?>

    <?php
    echo Comments\widgets\CommentListWidget::widget([
        'entity' => (string) $model->id,
    ]);
    ?>

</div>
