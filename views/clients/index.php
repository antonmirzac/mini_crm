<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить клиента', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'condensed'=>true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'email:email',
            'city',
            'main_feedback',
            'cellphone',
            'skype',
            'telegram',
            //'created_at',
            'date_next_contact',
            [
                'attribute' => 'customer_from',
                'value' => function($data){
                    return ($data->customerfrom) ? $data->customerfrom->title : "";
                },
            ],
            'loyalty',
            'vk',
            [
                'attribute' => 'info',
                'value' => function($data){
                    return StringHelper::truncate($data->info, 20);
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'Этапы',
                'value' => function($data){
                    $step = ($data->step_client_id) ? "Этап: " . $data->stepClient->title : "";
                    $board = ($data->step_client_id) ? "Доска: " . $data->stepClient->board->title : "";
                    return "
                                <div>{$step}</div>
                                <div>{$board}</div>
                            ";
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'contacts',
                'value' => function($data){
                    return \app\models\Clients::contactsArrToHtml($data->contacts);
                },
                'format' => 'html',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
