<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use app\components\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'customer_from')
                        ->dropDownList(\app\models\CustomerFrom::find()
                            ->select(['title', 'id'])
                            ->indexBy('id')
                            ->column(), ['prompt' => '']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2"><?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-2"><?= $form->field($model, 'main_feedback')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-2"><?= $form->field($model, 'cellphone')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-2"><?= $form->field($model, 'skype')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-2"><?= $form->field($model, 'telegram')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-2"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-2">
                    <?= $form->field($model, 'loyalty')->dropDownList(['0', '1', '2', '3', '4', '5'], ['prompt' => '']) ?>
                </div>
                <div class="col-md-2"><?= $form->field($model, 'vk')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-2">
                    <?= $form->field($model, 'step_client_id')
                        ->dropDownList(\app\models\Board::getBoardsWithSteps(), ['prompt' => 'Выберите этап']) ?>
                </div>


                <div class="col-md-6">
                    <?= $form->field($model, 'info')->textarea(['rows' => 1]) ?>
                </div>
                <div class="col-md-6">
                    <?php
                    echo $form->field($model, 'contacts')->widget(MultipleInput::className(), [
                        'max'               => 6,
                        'min'               => 1,
                        'allowEmptyList'    => true,
                        'enableGuessTitle'  => true,
                        'addButtonPosition' => MultipleInput::POS_ROW,

                        'columns' => [
                            [
                                'name'  => 'title',
                                'title' => 'Название',
                            ],
                            [
                                'name'  => 'text',
                                'title' => 'Текст',
                            ],
                        ]

                    ])
                        ->label(false);
                    ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'date_next_contact')->widget(
                DatePicker::className(), [
                'inline' => true,
                'language' => 'ru',
                'template' => '<div class="well well-sm">{input}</div>',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ],
            ]);?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
