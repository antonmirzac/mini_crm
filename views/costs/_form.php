<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Costs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="costs-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3"><?= $form->field($model, 'budget')->textInput(['maxlength' => true]) ?></div>
                <div class="col-md-3">
                    <?= $form->field($model, 'client_id')
                        ->dropDownList(\app\models\Clients::find()
                            ->select(['name', 'id'])
                            ->indexBy('id')
                            ->column(), ['prompt' => '']) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'expenditure_id')
                        ->dropDownList(\app\models\Expenditure::find()
                            ->select(['title', 'id'])
                            ->indexBy('id')
                            ->column(), ['prompt' => '']) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'type')
                        ->dropDownList(['Расход' => 'Расход', 'Доход' => 'Доход'], ['prompt' => '']) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
