<?php

use yii\db\Migration;

/**
 * Handles adding created_at to table `costs`.
 */
class m170407_111118_add_created_at_column_to_costs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('costs', 'created_at', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('costs', 'created_at');
    }
}
