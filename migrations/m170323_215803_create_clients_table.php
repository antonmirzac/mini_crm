<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients`.
 */
class m170323_215803_create_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'where_from' => $this->string(),
            'city' => $this->string(),
            'main_feedback' => $this->string(),
            'cellphone' => $this->string(),
            'skype' => $this->string(),
            'telegram' => $this->string(),
            'email' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('clients');
    }
}
