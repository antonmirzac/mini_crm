<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `user`.
 */
class m170323_211621_drop_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('user');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'password' => $this->string()->notNull(),
            'email' => $this->string()->unique(),
            'authKey' => $this->string(),
            'accessToken' => $this->string(),
        ]);
    }
}
