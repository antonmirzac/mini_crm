<?php

use yii\db\Migration;

/**
 * Handles adding updated_at to table `costs`.
 */
class m170407_111127_add_updated_at_column_to_costs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('costs', 'updated_at', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('costs', 'updated_at');
    }
}
