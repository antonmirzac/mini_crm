<?php

use yii\db\Migration;

/**
 * Handles the creation of table `costs`.
 * Has foreign keys to the tables:
 *
 * - `clients`
 * - `expenditure`
 */
class m170404_154313_create_costs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('costs', [
            'id' => $this->primaryKey(),
            'date' => $this->date(),
            'budget' => $this->string(),
            'client_id' => $this->integer(),
            'expenditure_id' => $this->integer(),
        ]);

        // creates index for column `client_id`
        $this->createIndex(
            'idx-costs-client_id',
            'costs',
            'client_id'
        );

        // add foreign key for table `clients`
        $this->addForeignKey(
            'fk-costs-client_id',
            'costs',
            'client_id',
            'clients',
            'id',
            'CASCADE'
        );

        // creates index for column `expenditure_id`
        $this->createIndex(
            'idx-costs-expenditure_id',
            'costs',
            'expenditure_id'
        );

        // add foreign key for table `expenditure`
        $this->addForeignKey(
            'fk-costs-expenditure_id',
            'costs',
            'expenditure_id',
            'expenditure',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `clients`
        $this->dropForeignKey(
            'fk-costs-client_id',
            'costs'
        );

        // drops index for column `client_id`
        $this->dropIndex(
            'idx-costs-client_id',
            'costs'
        );

        // drops foreign key for table `expenditure`
        $this->dropForeignKey(
            'fk-costs-expenditure_id',
            'costs'
        );

        // drops index for column `expenditure_id`
        $this->dropIndex(
            'idx-costs-expenditure_id',
            'costs'
        );

        $this->dropTable('costs');
    }
}
