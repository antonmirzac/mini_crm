<?php

use yii\db\Migration;

/**
 * Handles the creation of table `step_client`.
 * Has foreign keys to the tables:
 *
 * - `board`
 */
class m170411_092221_create_step_client_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('step_client', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'board_id' => $this->integer()->notNull(),
            'show_in_board' => $this->boolean()->defaultValue(0),
        ]);

        // creates index for column `board_id`
        $this->createIndex(
            'idx-step_client-board_id',
            'step_client',
            'board_id'
        );

        // add foreign key for table `board`
        $this->addForeignKey(
            'fk-step_client-board_id',
            'step_client',
            'board_id',
            'board',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `board`
        $this->dropForeignKey(
            'fk-step_client-board_id',
            'step_client'
        );

        // drops index for column `board_id`
        $this->dropIndex(
            'idx-step_client-board_id',
            'step_client'
        );

        $this->dropTable('step_client');
    }
}
