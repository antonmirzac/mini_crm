<?php

use yii\db\Migration;

/**
 * Handles dropping email from table `clients`.
 */
class m170404_085022_drop_email_column_from_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('clients', 'email');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
