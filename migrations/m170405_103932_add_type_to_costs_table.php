<?php

use yii\db\Migration;

class m170405_103932_add_type_to_costs_table extends Migration
{
    public function up()
    {
        $this->addColumn('costs', 'type', 'ENUM("Расход", "Доход")');
    }

    public function down()
    {
        echo "m170405_103932_add_type_to_costs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
