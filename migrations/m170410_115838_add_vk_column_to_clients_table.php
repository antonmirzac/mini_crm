<?php

use yii\db\Migration;

/**
 * Handles adding vk to table `clients`.
 */
class m170410_115838_add_vk_column_to_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'vk', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('clients', 'vk');
    }
}
