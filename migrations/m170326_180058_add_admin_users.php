<?php

use yii\db\Migration;

class m170326_180058_add_admin_users extends Migration
{
    private $defaultPassword = 'admin';

    public function up()
    {
        $this->batchInsert('user', ['email', 'username', 'password', 'candelete'], [
            ['admin@admin.com', 'admin', Yii::$app->security->generatePasswordHash($this->defaultPassword), 0],
            ['root@root.com', 'root', Yii::$app->security->generatePasswordHash($this->defaultPassword), 0],
        ]);
    }

    public function down()
    {
        echo "m170326_180058_add_admin_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
