<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customer_from`.
 */
class m170401_084617_create_customer_from_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer_from', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('customer_from');
    }
}
