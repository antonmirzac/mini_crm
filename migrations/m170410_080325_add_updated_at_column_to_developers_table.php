<?php

use yii\db\Migration;

/**
 * Handles adding updated_at to table `developers`.
 */
class m170410_080325_add_updated_at_column_to_developers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('developers', 'updated_at', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('developers', 'updated_at');
    }
}
