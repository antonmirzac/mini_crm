<?php

use yii\db\Migration;

/**
 * Handles the creation of table `board_order`.
 */
class m170419_113106_create_board_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('board_order', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('board_order');
    }
}
