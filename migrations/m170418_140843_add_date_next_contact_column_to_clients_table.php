<?php

use yii\db\Migration;

/**
 * Handles adding date_next_contact to table `clients`.
 */
class m170418_140843_add_date_next_contact_column_to_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'date_next_contact', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('clients', 'date_next_contact');
    }
}
