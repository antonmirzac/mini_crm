<?php

use yii\db\Migration;

/**
 * Handles adding created_at to table `developers`.
 */
class m170410_080317_add_created_at_column_to_developers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('developers', 'created_at', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('developers', 'created_at');
    }
}
