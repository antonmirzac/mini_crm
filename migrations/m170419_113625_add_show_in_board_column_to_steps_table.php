<?php

use yii\db\Migration;

/**
 * Handles adding show_in_board to table `steps`.
 */
class m170419_113625_add_show_in_board_column_to_steps_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('steps', 'show_in_board', $this->boolean()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('steps', 'show_in_board');
    }
}
