<?php

use yii\db\Migration;

/**
 * Handles adding loyalty to table `clients`.
 */
class m170406_122212_add_loyalty_column_to_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'loyalty', $this->integer()->unsigned());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('clients', 'loyalty');
    }
}
