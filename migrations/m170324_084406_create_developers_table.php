<?php

use yii\db\Migration;

/**
 * Handles the creation of table `developers`.
 */
class m170324_084406_create_developers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('developers', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'nick' => $this->string()->notNull()->unique(),
            'skype' => $this->string(),
            'phone' => $this->string(),
            'email' => $this->string(),
            'technologies' => $this->text(),
            'valuation' => $this->smallinteger()->unsigned(),
            'experience' => $this->smallinteger()->unsigned(),
            'mode_work' => $this->string(),
            'cost_of' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('developers');
    }
}
