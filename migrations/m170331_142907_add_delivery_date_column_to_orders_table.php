<?php

use yii\db\Migration;

/**
 * Handles adding delivery_date to table `orders`.
 */
class m170331_142907_add_delivery_date_column_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'delivery_date', $this->date());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'delivery_date');
    }
}
