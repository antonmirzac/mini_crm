<?php

use yii\db\Migration;

/**
 * Handles the creation of table `expenditure`.
 */
class m170404_153257_create_expenditure_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('expenditure', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('expenditure');
    }
}
