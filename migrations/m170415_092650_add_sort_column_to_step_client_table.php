<?php

use yii\db\Migration;

/**
 * Handles adding sort to table `step_client`.
 */
class m170415_092650_add_sort_column_to_step_client_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('step_client', 'sort', $this->smallInteger());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('step_client', 'sort');
    }
}
