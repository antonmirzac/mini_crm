<?php

use yii\db\Migration;

/**
 * Handles adding info to table `clients`.
 */
class m170410_115745_add_info_column_to_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'info', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('clients', 'info');
    }
}
