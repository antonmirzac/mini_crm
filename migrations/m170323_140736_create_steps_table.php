<?php

use yii\db\Migration;

/**
 * Handles the creation of table `steps`.
 */
class m170323_140736_create_steps_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('steps', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'sort' => $this->smallInteger()->unsigned(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('steps');
    }
}
