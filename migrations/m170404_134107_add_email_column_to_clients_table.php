<?php

use yii\db\Migration;

/**
 * Handles adding email to table `clients`.
 */
class m170404_134107_add_email_column_to_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'email', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('clients', 'email');
    }
}
