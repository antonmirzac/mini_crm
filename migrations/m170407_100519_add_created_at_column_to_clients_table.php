<?php

use yii\db\Migration;

/**
 * Handles adding created_at to table `clients`.
 */
class m170407_100519_add_created_at_column_to_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'created_at', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('clients', 'created_at');
    }
}
