<?php

use yii\db\Migration;

/**
 * Handles dropping customer_from from table `orders`.
 */
class m170401_105100_drop_customer_from_column_from_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        // drops foreign key for table `customer_from`
        $this->dropForeignKey(
            'fk-orders-customer_from',
            'orders'
        );

        // drops index for column `customer_from_id`
        $this->dropIndex(
            'idx-orders-customer_from',
            'orders'
        );
        $this->dropColumn('orders', 'customer_from');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
