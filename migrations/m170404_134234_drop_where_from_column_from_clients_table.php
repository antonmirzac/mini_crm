<?php

use yii\db\Migration;

/**
 * Handles dropping where_from from table `clients`.
 */
class m170404_134234_drop_where_from_column_from_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('clients', 'where_from');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('clients', 'where_from', $this->string());
    }
}
