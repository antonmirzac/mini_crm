<?php

use yii\db\Migration;

/**
 * Handles adding updated_at to table `technologies`.
 */
class m170410_084905_add_updated_at_column_to_technologies_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('technologies', 'updated_at', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('technologies', 'updated_at');
    }
}
