<?php

use yii\db\Migration;

class m170426_204316_add_unique_index_to_orders_title extends Migration
{
    public function up()
    {
        $this->createIndex('title', 'orders', 'title', $unique = true );
    }

    public function down()
    {
        echo "m170426_204316_add_unique_index_to_orders_title cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
