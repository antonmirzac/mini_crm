<?php

use yii\db\Migration;

class m170401_085049_add_foreign_key_to_customer_from extends Migration
{
    public function up()
    {
        $this->alterColumn('orders','customer_from','integer');

        // creates index for column `customer_from_id`
        $this->createIndex(
            'idx-orders-customer_from',
            'orders',
            'customer_from'
        );

        // add foreign key for table `customer_from`
        $this->addForeignKey(
            'fk-orders-customer_from',
            'orders',
            'customer_from',
            'customer_from',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        // drops foreign key for table `customer_from`
        $this->dropForeignKey(
            'fk-orders-customer_from',
            'orders'
        );

        // drops index for column `customer_from_id`
        $this->dropIndex(
            'idx-orders-customer_from',
            'orders'
        );

        $this->alterColumn('orders','customer_from','string');
    }


    // Use safeUp/safeDown to run migration code within a transaction
    /*
    public function safeUp()
    {

    }


    public function safeDown()
    {
    }
    */
}
