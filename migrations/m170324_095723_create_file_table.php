<?php

use yii\db\Migration;

/**
 * Handles the creation of table `file`.
 * Has foreign keys to the tables:
 *
 * - `orders`
 */
class m170324_095723_create_file_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('file', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'title' => $this->string(),
            'path' => $this->text(),
        ]);

        // creates index for column `order_id`
        $this->createIndex(
            'idx-file-order_id',
            'file',
            'order_id'
        );

        // add foreign key for table `orders`
        $this->addForeignKey(
            'fk-file-order_id',
            'file',
            'order_id',
            'orders',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `orders`
        $this->dropForeignKey(
            'fk-file-order_id',
            'file'
        );

        // drops index for column `order_id`
        $this->dropIndex(
            'idx-file-order_id',
            'file'
        );

        $this->dropTable('file');
    }
}
