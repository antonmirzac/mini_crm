<?php

use yii\db\Migration;

/**
 * Handles the creation of table `technologies`.
 */
class m170323_215924_create_technologies_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('technologies', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('technologies');
    }
}
