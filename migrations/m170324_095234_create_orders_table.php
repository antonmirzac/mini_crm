<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 * Has foreign keys to the tables:
 *
 * - `clients`
 * - `developers`
 * - `steps`
 * - `user`
 */
class m170324_095234_create_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'project_link' => $this->string(),
            'accesses' => $this->text(),
            'full_sum' => $this->integer(),
            'clear_sum' => $this->integer(),
            'payed_sum' => $this->integer(),
            'count_steps' => $this->smallinteger()->unsigned(),
            'created_at' => $this->datetime(),
            'updated_at' => $this->datetime(),
            'client_id' => $this->integer(),
            'developer_id' => $this->integer(),
            'step_id' => $this->integer(),
            'observer_id' => $this->integer(),
        ]);

        // creates index for column `client_id`
        $this->createIndex(
            'idx-orders-client_id',
            'orders',
            'client_id'
        );

        // add foreign key for table `clients`
        $this->addForeignKey(
            'fk-orders-client_id',
            'orders',
            'client_id',
            'clients',
            'id',
            'CASCADE'
        );

        // creates index for column `developer_id`
        $this->createIndex(
            'idx-orders-developer_id',
            'orders',
            'developer_id'
        );

        // add foreign key for table `developers`
        $this->addForeignKey(
            'fk-orders-developer_id',
            'orders',
            'developer_id',
            'developers',
            'id',
            'CASCADE'
        );

        // creates index for column `step_id`
        $this->createIndex(
            'idx-orders-step_id',
            'orders',
            'step_id'
        );

        // add foreign key for table `steps`
        $this->addForeignKey(
            'fk-orders-step_id',
            'orders',
            'step_id',
            'steps',
            'id',
            'CASCADE'
        );

        // creates index for column `observer_id`
        $this->createIndex(
            'idx-orders-observer_id',
            'orders',
            'observer_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-orders-observer_id',
            'orders',
            'observer_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `clients`
        $this->dropForeignKey(
            'fk-orders-client_id',
            'orders'
        );

        // drops index for column `client_id`
        $this->dropIndex(
            'idx-orders-client_id',
            'orders'
        );

        // drops foreign key for table `developers`
        $this->dropForeignKey(
            'fk-orders-developer_id',
            'orders'
        );

        // drops index for column `developer_id`
        $this->dropIndex(
            'idx-orders-developer_id',
            'orders'
        );

        // drops foreign key for table `steps`
        $this->dropForeignKey(
            'fk-orders-step_id',
            'orders'
        );

        // drops index for column `step_id`
        $this->dropIndex(
            'idx-orders-step_id',
            'orders'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-orders-observer_id',
            'orders'
        );

        // drops index for column `observer_id`
        $this->dropIndex(
            'idx-orders-observer_id',
            'orders'
        );

        $this->dropTable('orders');
    }
}
