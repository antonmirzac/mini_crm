<?php

use yii\db\Migration;

/**
 * Handles adding board_id to table `steps`.
 * Has foreign keys to the tables:
 *
 * - `board_order`
 */
class m170419_113614_add_board_id_column_to_steps_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('steps', 'board_id', $this->integer());

        // creates index for column `board_id`
        $this->createIndex(
            'idx-steps-board_id',
            'steps',
            'board_id'
        );

        // add foreign key for table `board_order`
        $this->addForeignKey(
            'fk-steps-board_id',
            'steps',
            'board_id',
            'board_order',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `board_order`
        $this->dropForeignKey(
            'fk-steps-board_id',
            'steps'
        );

        // drops index for column `board_id`
        $this->dropIndex(
            'idx-steps-board_id',
            'steps'
        );

        $this->dropColumn('steps', 'board_id');
    }
}
