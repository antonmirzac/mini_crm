<?php

use yii\db\Migration;

/**
 * Handles adding updated_at to table `clients`.
 */
class m170407_100529_add_updated_at_column_to_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'updated_at', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('clients', 'updated_at');
    }
}
