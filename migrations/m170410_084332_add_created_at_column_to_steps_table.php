<?php

use yii\db\Migration;

/**
 * Handles adding created_at to table `steps`.
 */
class m170410_084332_add_created_at_column_to_steps_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('steps', 'created_at', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('steps', 'created_at');
    }
}
