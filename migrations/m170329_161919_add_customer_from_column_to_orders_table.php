<?php

use yii\db\Migration;

/**
 * Handles adding customer_from to table `orders`.
 */
class m170329_161919_add_customer_from_column_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'customer_from', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'customer_from');
    }
}
