<?php

use yii\db\Migration;

/**
 * Handles adding candelete to table `user`.
 */
class m170326_170522_add_candelete_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'candelete', $this->smallInteger()->notNull()->defaultValue('1'));
    }

    //"ENUM('yes', 'no')"

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'candelete');
    }
}
