<?php

use yii\db\Migration;

class m170426_204826_add_unique_index_to_clients_name extends Migration
{
    public function up()
    {
        $this->createIndex('clients_name_idx', 'clients', 'name', $unique = true );
    }

    public function down()
    {
        echo "m170426_204826_add_unique_index_to_clients_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
