<?php

use yii\db\Migration;

/**
 * Handles adding customer_from to table `clients`.
 * Has foreign keys to the tables:
 *
 * - `customer_from`
 */
class m170401_105624_add_customer_from_column_to_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'customer_from', $this->integer());

        // creates index for column `customer_from`
        $this->createIndex(
            'idx-clients-customer_from',
            'clients',
            'customer_from'
        );

        // add foreign key for table `customer_from`
        $this->addForeignKey(
            'fk-clients-customer_from',
            'clients',
            'customer_from',
            'customer_from',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `customer_from`
        $this->dropForeignKey(
            'fk-clients-customer_from',
            'clients'
        );

        // drops index for column `customer_from`
        $this->dropIndex(
            'idx-clients-customer_from',
            'clients'
        );

        $this->dropColumn('clients', 'customer_from');
    }
}
