<?php

use yii\db\Migration;

/**
 * Handles adding updated_at to table `steps`.
 */
class m170410_084340_add_updated_at_column_to_steps_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('steps', 'updated_at', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('steps', 'updated_at');
    }
}
