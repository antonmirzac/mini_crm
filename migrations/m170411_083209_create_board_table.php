<?php

use yii\db\Migration;

/**
 * Handles the creation of table `board`.
 */
class m170411_083209_create_board_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('board', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('board');
    }
}
