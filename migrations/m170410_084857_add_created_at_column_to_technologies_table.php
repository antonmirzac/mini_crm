<?php

use yii\db\Migration;

/**
 * Handles adding created_at to table `technologies`.
 */
class m170410_084857_add_created_at_column_to_technologies_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('technologies', 'created_at', $this->datetime());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('technologies', 'created_at');
    }
}
