<?php

use yii\db\Migration;

/**
 * Handles adding step_client_id to table `clients`.
 * Has foreign keys to the tables:
 *
 * - `step_client`
 */
class m170411_094004_add_step_client_id_column_to_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'step_client_id', $this->integer());

        // creates index for column `step_client_id`
        $this->createIndex(
            'idx-clients-step_client_id',
            'clients',
            'step_client_id'
        );

        // add foreign key for table `step_client`
        $this->addForeignKey(
            'fk-clients-step_client_id',
            'clients',
            'step_client_id',
            'step_client',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `step_client`
        $this->dropForeignKey(
            'fk-clients-step_client_id',
            'clients'
        );

        // drops index for column `step_client_id`
        $this->dropIndex(
            'idx-clients-step_client_id',
            'clients'
        );

        $this->dropColumn('clients', 'step_client_id');
    }
}
