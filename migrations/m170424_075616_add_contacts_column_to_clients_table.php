<?php

use yii\db\Migration;

/**
 * Handles adding contacts to table `clients`.
 */
class m170424_075616_add_contacts_column_to_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('clients', 'contacts', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('clients', 'contacts');
    }
}
