<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "costs".
 *
 * @property integer $id
 * @property string $date
 * @property string $budget
 * @property integer $client_id
 * @property integer $expenditure_id
 *
 * @property Clients $client
 * @property Expenditure $expenditure
 */
class Costs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'costs';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
            [['client_id', 'expenditure_id'], 'integer'],
            [['budget', 'type'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['expenditure_id'], 'exist', 'skipOnError' => true, 'targetClass' => Expenditure::className(), 'targetAttribute' => ['expenditure_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'budget' => 'Бюджет РК внесено',
            'client_id' => 'Клиент',
            'expenditure_id' => 'Статья',
            'type' => 'Тип(Расход/Доход)',
            'created_at' => 'Дата',
            'updated_at' => 'Дата изменения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditure()
    {
        return $this->hasOne(Expenditure::className(), ['id' => 'expenditure_id']);
    }
}
