<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "board_order".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Steps[] $steps
 */
class BoardOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'board_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSteps()
    {
        return $this->hasMany(Steps::className(), ['board_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getStepsWithOrders($id) {

        $rows = (new \yii\db\Query())
            ->select([
                's.id as step_id',
                's.title as step_title',
                'o.id as order_id',
                'o.title as order_title',
                'o.full_sum as full_sum',
                'o.delivery_date as delivery_date',
                'o.clear_sum as clear_sum',
                'c.name as client_name',
                'c.loyalty as client_loyalty',
                'c.id as client_id',
                'd.nick as developer_name',
                'CURDATE() as today',
                'DATEDIFF(delivery_date,CURDATE()) as diff_date',
            ])
            ->from('steps s')
            ->join('LEFT JOIN', 'orders o', 's.id = o.step_id')
            ->join('LEFT JOIN', 'clients c', 'o.client_id = c.id')
            ->join('LEFT JOIN', 'developers d', 'o.developer_id = d.id')
            ->where(['s.board_id' => $id])
            ->andwhere(['s.show_in_board' => 1])
            ->orderBy(['sort' => SORT_ASC])
            ->all();

        $result = [];
        foreach ($rows as $item) {
            $stepId = $item['step_id'];
            $result[$stepId]['title'] = $item['step_title'];
            $result[$stepId]['id'] = $item['step_id'];
            if(!isset($result[$stepId]['all_sum'])) {
                $result[$stepId]['all_sum'] = 0;
            }
            $result[$stepId]['all_sum'] += $item['full_sum'];
            if(!empty($item['order_id'])) {
                $diff_date = $item['diff_date'];

                if($diff_date <= 2 && $diff_date > 0 && $diff_date) {
                    $deadline = 'yellow';
                } else if($diff_date <= 0 && $diff_date) {
                    $deadline = 'red';
                } else {
                    $deadline = 'default';
                }

                $item['deadline'] = $deadline;
                $result[$stepId]['orders'][] = $item;
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public static function getBoardsWithSteps()
    {
        $rows = self::find()
            ->with('steps')
            ->indexBy('title')
            ->asArray()
            ->all();

        $result = [];
        if(!empty($rows)) {
            foreach ($rows as $key => $itemBoard) {
                if(empty($itemBoard['steps'])) {
                    $result[$key] = [];
                } else {
                    foreach ($itemBoard['steps'] as $itemStep) {
                        $stepId = $itemStep['id'];
                        $title = $itemStep['title'];
                        $result[$key][$stepId] = $title;
                    }
                }

            }
        }
        return $result;
    }
}
