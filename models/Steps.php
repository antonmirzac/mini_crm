<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "steps".
 *
 * @property integer $id
 * @property string $title
 * @property integer $sort
 *
 * @property Orders[] $orders
 */
class Steps extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'steps';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'board_id'], 'required'],
            [['sort', 'board_id', 'show_in_board'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['created_at', 'updated_at'], 'safe'],
            [['board_id'], 'exist', 'skipOnError' => true, 'targetClass' => BoardOrder::className(), 'targetAttribute' => ['board_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'sort' => 'Индекс сортировки',
            'board_id' => 'Наименование доски',
            'show_in_board' => 'Показывать на доске',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['step_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoard()
    {
        return $this->hasOne(BoardOrder::className(), ['id' => 'board_id']);
    }

    /**
     * @return array
     */
    public function getOrdersWithSteps() {

        $rows = (new \yii\db\Query())
            ->select([
                's.id as step_id',
                's.title as step_title',
                'o.id as order_id',
                'o.title as order_title',
                'o.full_sum as full_sum',
                'o.delivery_date as delivery_date',
                'o.clear_sum as clear_sum',
                'c.name as client_name',
                'c.loyalty as client_loyalty',
                'c.id as client_id',
                'd.nick as developer_name',
                'CURDATE() as today',
                'DATEDIFF(delivery_date,CURDATE()) as diff_date',
            ])
            ->from('steps s')
            ->join('LEFT JOIN', 'orders o', 's.id = o.step_id')
            ->join('LEFT JOIN', 'clients c', 'o.client_id = c.id')
            ->join('LEFT JOIN', 'developers d', 'o.developer_id = d.id')
            ->orderBy(['sort' => SORT_ASC])
            ->all();

        $result = [];
        foreach ($rows as $item) {
            $stepId = $item['step_id'];
            $result[$stepId]['title'] = $item['step_title'];
            $result[$stepId]['id'] = $item['step_id'];
            if(!isset($result[$stepId]['all_sum'])) {
                $result[$stepId]['all_sum'] = 0;
            }
            $result[$stepId]['all_sum'] += $item['full_sum'];
            if(!empty($item['order_id'])) {
                $diff_date = $item['diff_date'];

                if($diff_date <= 2 && $diff_date > 0 && $diff_date) {
                    $deadline = 'yellow';
                } else if($diff_date <= 0 && $diff_date) {
                    $deadline = 'red';
                } else {
                    $deadline = 'default';
                }

                $item['deadline'] = $deadline;
                $result[$stepId]['orders'][] = $item;
            }
        }

        return $result;
    }
}
