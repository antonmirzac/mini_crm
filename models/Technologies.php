<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "technologies".
 *
 * @property integer $id
 * @property string $title
 */
class Technologies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'technologies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * Get select techologies titles by json string
     *
     * @param $json string Json string
     * @return \yii\db\ActiveRecord[]
     */
    public function getByJson($json)
    {
        $arr = json_decode($json, true);
        $arr = $this->find()->where(['id' => $arr])->asArray()->all();
        $res = [];
        foreach ($arr as $item) {
            array_push($res, $item['id']);
        }

        return $res;
    }

    /**
     * Get techologies titles by json string. Static method
     *
     * @param $json string Json string
     * @return \yii\db\ActiveRecord[]
     */
    public static function getByJsonStatic($json)
    {
        $arr = json_decode($json, true);
        $arr = static::find()->where(['id' => $arr])->asArray()->all();
        $res = [];
        foreach ($arr as $item) {
            array_push($res, $item['id']);
        }

        return $res;
    }

    /**
     * Return all technologies
     *
     * @return \yii\db\ActiveRecord[]
     */
    public function getAll()
    {
        return $this->find()->select(['title', 'id'])->indexBy('id')->column();
    }

    /**
     * Return titles fot select technologies
     *
     * @param $arr array Select technologies id
     * @return array
     */
    public static function getTitles($arr)
    {
        return self::find()->select(['title', 'id'])->where(['id' => $arr])->indexBy('id')->column();
    }
}
