<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_from".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Orders[] $orders
 */
class CustomerFrom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_from';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Ссылка/Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Clients::className(), ['customer_from' => 'id']);
    }
}
