<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "step_client".
 *
 * @property integer $id
 * @property string $title
 * @property integer $board_id
 * @property integer $show_in_board
 *
 * @property Board $board
 */
class StepClient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'step_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'board_id'], 'required'],
            [['board_id', 'show_in_board','sort'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['board_id'], 'exist', 'skipOnError' => true, 'targetClass' => Board::className(), 'targetAttribute' => ['board_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'board_id' => 'Доска',
            'show_in_board' => 'Показывать на доске',
            'sort' => 'Индекс сортировки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoard()
    {
        return $this->hasOne(Board::className(), ['id' => 'board_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['step_client_id' => 'id']);
    }
}
