<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\UploadedFile;
use app\models\File;
use yii\db\Query;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property string $title
 * @property string $project_link
 * @property string $accesses
 * @property integer $full_sum
 * @property integer $clear_sum
 * @property integer $payed_sum
 * @property integer $count_steps
 * @property string $created_at
 * @property string $updated_at
 * @property integer $client_id
 * @property integer $developer_id
 * @property integer $step_id
 * @property integer $observer_id
 *
 * @property File[] $files
 * @property Clients $client
 * @property Developers $developer
 * @property User $observer
 * @property Steps $step
 */
class Orders extends ActiveRecord
{
    /**
     * @var UploadedFile[]
     */
    //public $files;

    /**
     * @var string Temp files json
     */
    public $tempFiles;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['accesses'], 'string'],
            [['full_sum', 'clear_sum', 'payed_sum', 'count_steps', 'client_id', 'developer_id', 'step_id', 'observer_id'], 'integer'],
            [['created_at', 'updated_at', 'delivery_date'], 'safe'],
            [['title', 'project_link'], 'string', 'max' => 255],
            [['title'], 'unique'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['developer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Developers::className(), 'targetAttribute' => ['developer_id' => 'id']],
            [['observer_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['observer_id' => 'id']],
            [['step_id'], 'exist', 'skipOnError' => true, 'targetClass' => Steps::className(), 'targetAttribute' => ['step_id' => 'id']],
            [['files'], 'file', 'skipOnEmpty' => true, 'maxFiles' => 4, 'maxSize' => 20971520],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'project_link' => 'Ссылка на проект',
            'accesses' => 'Доступы к проекту(текстовый)',
            'full_sum' => 'Сумма общая',
            'clear_sum' => 'Сумма чистая',
            'payed_sum' => 'Сумма оплаченная',
            'count_steps' => 'Кол. этапов',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'files' => 'Файлы',
            'client_id' => 'Клиент',
            'developer_id' => 'Исполнитель',
            'step_id' => 'Этап',
            'observer_id' => 'Наблюдатель',
            'tempFiles' => 'Файлы',
            'delivery_date' => 'Дата сдачи',
            'sum_group' => 'Суммы',
            'participants' => 'Участники',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeveloper()
    {
        return $this->hasOne(Developers::className(), ['id' => 'developer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObserver()
    {
        return $this->hasOne(User::className(), ['id' => 'observer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStep()
    {
        return $this->hasOne(Steps::className(), ['id' => 'step_id']);
    }

    /**
     * Return max order id
     *
     * @return integer Max order id  + 1
     */
    public function getLastId()
    {
        $maxId = (new \yii\db\Query())
            ->select('auto_increment')
            ->from('information_schema.tables')
            ->where(['table_name' => 'orders'])
            ->one();
        return $maxId['auto_increment'];
    }

    /**
     * Writes rows to a table File
     *
     * @param \app\models\File $file
     * @param $tempFiles array
     * @param $order_id integer
     */
    public function addFiles($tempFiles, $order_id) {
        if(is_null($tempFiles)) {
            return false;
        }
        foreach ($tempFiles as $itemFile) {
            $file = new File();
            $file->order_id = $order_id;
            $file->title = $itemFile['name'];
            $file->path = $itemFile['path'];
            $file->save();
            unset($file);
        }
    }

    /**
     * Return uniques customer from column values
     *
     * @return array|ActiveRecord[]
     */
    /*
    public function getCustomerFrom() {
        $subQuery = (new Query())->select('COUNT(customer_from) AS total');
        return $query = (new Query())
            ->select(['customer_from', 'total' => $subQuery])
            ->from('orders')
            ->groupBy('customer_from')
            ->orderBy(['total' => SORT_DESC])
            ->all();
    }
    */
}
