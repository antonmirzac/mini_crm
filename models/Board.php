<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "board".
 *
 * @property integer $id
 * @property string $title
 */
class Board extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'board';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
        ];
    }

    /**
     * @param $id integer Board 1
     * @return array
     */
    public function getStepsWithClients($id)
    {
        $rows = (new \yii\db\Query())
            ->select([
                'b.title as board_title',
                'sc.title as step_client_title',
                'sc.id as step_client_id',
                'c.id as client_id',
                'c.name as client_name',
                'c.cellphone as client_cellphone',
                '(SELECT COUNT(*) FROM orders WHERE client_id = c.id) as count_orders',
                'DATEDIFF(date_next_contact,CURDATE()) as diff_date',
                'date_next_contact',
            ])
            ->from('board b')
            ->join('LEFT JOIN', 'step_client sc', 'b.id = sc.board_id')
            ->join('LEFT JOIN', 'clients c', 'sc.id = c.step_client_id')
            ->where(['b.id' => $id])
            ->andwhere(['sc.show_in_board' => 1])
            ->orderBy(['sc.sort' => SORT_ASC])
            ->all();

        $result = [];
        if(!empty($rows)) {
            $result['title'] = $rows[0]['board_title'];
            $result['id'] = $id;

            foreach ($rows as $item) {
                $step_client_id = $item['step_client_id'];
                $result['items'][$step_client_id]['title'] = $item['step_client_title'];
                $result['items'][$step_client_id]['id'] = $step_client_id;
                if(!empty($item['client_id'])) {

                    $diff_date = $item['diff_date'];

                    if($diff_date <= 2 && $diff_date > 0 && $diff_date) {
                        $deadline = 'yellow';
                    } else if($diff_date <= 0 && $diff_date) {
                        $deadline = 'red';
                    } else {
                        $deadline = 'default';
                    }

                    $item['deadline'] = $deadline;
                    $result['items'][$step_client_id]['clients'][] = $item;
                }
            }
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getBoardsWithSteps()
    {
        $rows = self::find()
            ->with('stepClients')
            ->indexBy('title')
            ->asArray()
            ->all();

        $result = [];
        if(!empty($rows)) {
            foreach ($rows as $key => $itemBoard) {
                if(empty($itemBoard['stepClients'])) {
                    $result[$key] = [];
                } else {
                    foreach ($itemBoard['stepClients'] as $itemStep) {
                        $stepId = $itemStep['id'];
                        $title = $itemStep['title'];
                        $result[$key][$stepId] = $title;
                    }
                }

            }
        }
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStepClients()
    {
        return $this->hasMany(StepClient::className(), ['board_id' => 'id']);
    }
}
