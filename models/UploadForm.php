<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $anyFiles;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['anyFiles'], 'file', 'skipOnEmpty' => false, 'maxFiles' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'anyFiles' => 'Файлы',
        ];
    }

    /**
     * Ajax upload images
     *
     * @param Directory $directory
     * @param $order_id
     * @return bool|string
     */
    public function upload(Directory $directory, $order_id)
    {
        if ($this->validate()) {

            $path = $directory->createDirectory($order_id);

            $file = $this->anyFiles[0];
            $filename = Yii::$app->security->generateRandomString(8) . '.' . $file->extension;
            $file->saveAs($path . $filename);

            return $path . $filename;
        } else {
            return false;
        }
    }
}