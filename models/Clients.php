<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use \yii\db\ActiveRecord;
use yii\base\Event;
//use yii\base\View;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $name
 * @property string $where_from
 * @property string $city
 * @property string $main_feedback
 * @property string $cellphone
 * @property string $skype
 * @property string $telegram
 * @property string $email
 *
 * @property Orders[] $orders
 */
class Clients extends ActiveRecord
{
    /**
     * @var integer Board id
     */
    public $board;

    //public $schedule;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    public function init()
    {
        Event::on(ActiveRecord::className(), ActiveRecord::EVENT_BEFORE_VALIDATE, function ($event) {
            $this->encodeContacts();
        });

        Event::on(ActiveRecord::className(), ActiveRecord::EVENT_AFTER_FIND, function ($event) {
            $this->decodeContacts();
        });
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['name', 'city', 'main_feedback', 'cellphone', 'skype', 'telegram', 'vk',], 'string', 'max' => 255],
            [['info', 'contacts'], 'string'],
            [['loyalty'], 'integer'],
            [['created_at', 'updated_at', 'date_next_contact'], 'safe'],
            ['email', 'email'],
            [['customer_from'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerFrom::className(), 'targetAttribute' => ['customer_from' => 'id']],
            [['step_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => StepClient::className(), 'targetAttribute' => ['step_client_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'city' => 'Город',
            'main_feedback' => 'Основная связь',
            'cellphone' => 'Сотовый',
            'skype' => 'Skype',
            'telegram' => 'Telegram',
            'customer_from' => 'Откуда пришел клиент',
            'email' => 'E-mail',
            'loyalty' => 'Лояльность',
            'vk' => 'vk ссылка',
            'info' => 'Доп. информация',
            'step_client_id' => 'Этап',
            'board' => 'Доска',
            'date_next_contact' => 'Дата сл. контакта',
            'contacts' => 'Контакты',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerfrom()
    {
        return $this->hasOne(CustomerFrom::className(), ['id' => 'customer_from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStepClient()
    {
        return $this->hasOne(StepClient::className(), ['id' => 'step_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCosts()
    {
        return $this->hasMany(Costs::className(), ['client_id' => 'id']);
    }

    /**
     * Convert array contacts column to json format
     */
    public function encodeContacts()
    {
        $this->contacts = json_encode($this->contacts);
    }

    /**
     * Convert json contacts column to array format
     */
    public function decodeContacts()
    {
        if(!is_null($this->contacts) && is_string($this->contacts)) {
            $jsonArr = json_decode($this->contacts);
            $resArr = [];
            foreach ($jsonArr as $key => $item) {
                $resArr[$key]['title'] = $item->title;
                $resArr[$key]['text'] = $item->text;
            }
            $this->contacts = $resArr;
        }
    }

    public static function contactsArrToHtml($contacts)
    {
        $res = "";
        if(is_null($contacts)) {
            return $res;
        } else {
            foreach ($contacts as $item) {
                if(!empty($item['title']) || !empty($item['text'])) {
                    $res .=  $item['title'] . ":" . $item['text'] . "<br>";
                }

            }
        }

        return $res;
    }
}
