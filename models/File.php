<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $title
 * @property string $path
 *
 * @property Orders $order
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id'], 'integer'],
            [['path'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            //[['file'], 'file', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'title' => 'Название',
            'path' => 'Путь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @param $orderId integer
     * @return array
     */
    public static function getPathes($orderId) {
        $pathes = self::find()->select('path')->asArray()->all();
        $res = [];
        foreach ($pathes as $path) {
            $res[] = DIRECTORY_SEPARATOR . $path['path'];
        }

        return $res;
    }

    public static function getFilesContent($orderId) {
        $resArr = self::find()->where(['order_id' => $orderId])->asArray()->all();

        $str = "<div class='files-content'>";
        foreach ($resArr as $item) {
            $id = $item['id'];
            $title = $item['title'];
            $path = Url::to(['orders/download', 'path' => $item['path']]);
            $removeUrl = Url::to(['/ajax/remove-file', 'id' => $id]);
            $str .= "<div class='file-item'>
                        <a href='{$path}' id='{$id}' class='filename'>{$title}</a>
                        <a href='{$removeUrl}' class='remove-file'><i class='fa fa-times' aria-hidden='true'></i></a>
                     </div>";
        }

        $str .= "</div>";

        return $str;
    }
}
