<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "developers".
 *
 * @property integer $id
 * @property string $name
 * @property string $nick
 * @property string $skype
 * @property string $phone
 * @property string $email
 * @property string $technologies
 * @property integer $valuation
 * @property integer $experience
 * @property string $mode_work
 * @property string $cost_of
 *
 * @property Orders[] $orders
 */
class Developers extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $selectTechnologies = null;

    /**
     * @var array
     */
    public $allTechnologies = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'developers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nick'], 'required'],
            [['technologies'], 'string'],
            [['valuation', 'experience'], 'integer'],
            [['name', 'nick', 'skype', 'phone', 'mode_work', 'cost_of'], 'string', 'max' => 255],
            [['nick'], 'unique'],
            ['email', 'email'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'nick' => 'Ник',
            'skype' => 'Skype',
            'phone' => 'Телефон',
            'email' => 'Email',
            'technologies' => 'Технологии',
            'valuation' => 'Оценка',
            'experience' => 'Опыт в годах',
            'mode_work' => 'Режим работы',
            'cost_of' => 'Стоимость часа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['developer_id' => 'id']);
    }
}
