<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 5/6/17
 * Time: 11:54 PM
 */

namespace app\assets;

use yii\web\AssetBundle;

class AjaxClientsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/ajax-clients/app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}